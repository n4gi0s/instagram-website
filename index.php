<?php
require("InstagramUser.php");
require("config.php");
$user = new InstagramUser($INSTAGRAM_USERID);
$profil = $user->getInfo();
?>

<!DOCTYPE HTML>
<!--
	Lens by HTML5 UP
	html5up.net | @ajlkn
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<!--
	Automated Instagram Crawler by @N4gi0s
-->
<html>
	<head>
		<title><?php echo $TITLE; ?></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<noscript><link rel="stylesheet" href="assets/css/noscript.css" /></noscript>
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo $profil->profile_pic_url; ?>">
	</head>
	<body class="is-preload-0 is-preload-1 is-preload-2">

		<!-- Main -->
			<div id="main">

				<!-- Header -->
					<header id="header">
						<h1><?php echo $TITLE; ?></h1>
						<ul class="icons">
							<li><a href="https://twitter.com/<?php echo $TWITTER_USER; ?>" class="icon brands fa-twitter"><span class="label">Twitter</span></a></li>
							<li><a href="https://gitlab.com/<?php echo $GITLAB_USER; ?>" class="icon brands fa-gitlab"><span class="label">GitLab</span></a></li>
							<li><a href="https://www.instagram.com/<?php echo $profil->username; ?>/" class="icon brands fa-instagram"><span class="label">Instagram</span></a></li>
						</ul>
					</header>

				<!-- Thumbnail -->
					<section id="thumbnails">
					<?php foreach($user->getPosts(null, $INSTAGRAM_POSTCOUNT)->edges as $post) { $node = $post->node; ?>
						<article>
							<a class="thumbnail" href="<?php echo $node->display_url; ?>" data-position="left center"><img src="<?php echo $node->thumbnail_src; ?>" alt="" /></a>
							<p><?php
								$edges = $post->node->edge_media_to_caption->edges;
								if(sizeof($edges) > 0) echo(str_replace("\n", "<br>", $edges[0]->node->text));
							?></p>
						</article>
					<?php } ?>
					</section>

				<!-- Footer -->
					<footer id="footer">
						<ul class="copyright">
							<li>&copy; <?php echo $COPYRIGHT; ?>.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a>.</li>
						</ul>
					</footer>

			</div>

		<!-- Scripts -->
			<script src="assets/js/jquery.min.js"></script>
			<script src="assets/js/browser.min.js"></script>
			<script src="assets/js/breakpoints.min.js"></script>
			<script src="assets/js/main.js"></script>

	</body>
</html>