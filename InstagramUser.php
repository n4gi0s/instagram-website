<?php
class InstagramUser
{     
    public $userid;
    function __construct($userid) {
        $this->userid = $userid;
    }

    private static function queryGraphql($vars, $hash) {
        $url = "https://www.instagram.com/graphql/query/?query_hash=" . $hash . "&variables=" . json_encode($vars);
        $res = file_get_contents($url);
        return json_decode($res)->data->user;
    }

    public function getInfo() {
        $vars = [
            "user_id" => $this->userid,
            "include_chaining" => false,
            "include_reel" => true,
            "include_suggested_users" => false,
            "include_logged_out_extras" => false,
            "include_highlight_reels" => false
        ];
        return $this->queryGraphql($vars, 'aec5501414615eca36a9acf075655b1e')->reel->owner;
    }

    public function getPosts($after = null, $count = 12) {
        $vars = [
            "id" => $this->userid,
            "first" => $count,
            "after" => $after
        ];
        return $this->queryGraphql($vars, '58b6785bea111c67129decbe6a448951')->edge_owner_to_timeline_media;
    }
}